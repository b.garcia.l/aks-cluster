## Building the hello-world-deployer

```
echo "deployer:$DEPLOYED_PASSWORD" > deployer_password.txt
export DOCKER_BUILDKIT=1
```

```
> docker build --target final --secret id=deployer_password,src=deployer_password.txt -t hello-world-deployer:1.0.0 .