# Context

This repository presents:
- A modular terraform workflow approach for provisioning an AKS cluster
- A Gitlab pipeline that executes the terraform workflow. 
- A custom `hello-world-deployer` docker image was created in the spirit of gathering the following tools
    - terraform
    - kubectl
    - azure cli
    - kubelogin
    - helm

## Deploying the AKS cluster from gitlab
A simple gitlab pipeline was created to deploy the cluster:

- **build** stage: `terraform fmt` and `terraform validate`
- **deploy** stage: For running `terraform apply` in order to:
    - Create the AKS cluster. It was created with one nodepool and one workernode.
    - Create the `development` namespace.
    - Deploy the [hello-kubernetes](https://github.com/paulbouwer/hello-kubernetes) application with three replicas.
  
- **runtime** stage: To check the pods running and getting their events. [Here](https://gitlab.com/b.garcia.l/aks-cluster/-/jobs/5469503900#L103) you can see the events of the pods

- **logging** stage: To get the logs of the pods. The logs were stored into a file and presented in this stage.
[Here](https://gitlab.com/b.garcia.l/aks-cluster/-/jobs/5469503922#L45) you can see the logs of the pods.

## Requirements to execute the solution

1. A service principal should be created previously. It will be used to authenticate to azure and
allow terraform operations. 

```
> az ad sp create-for-rbac \
  --display-name "AKS Deployment Service Principal" \
  -n "aks-deployment" \    
  --json-auth true \
  --role Contributor \
  --scopes /subscriptions/9148bd11-f32b-4b5d-a6c0-5ac5317f29ca 
```

2. Storage account and blob container to save the terraform state file. It is done via [`aks-cluster/scripts/create-sa-blob-container.sh`](https://gitlab.com/b.garcia.l/aks-cluster/-/blob/main/scripts/create-sa-blob-container.sh) script

3. When deploying to a specific environment, this terraform workflow is working with terraform workspaces.
It means that a terraform workspace should be created before applying `terraform plan | apply`

For example for staging environment:
```
> terraform workspace new stag
Created and switched to workspace "stag"!

You're now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration.
```

If no terraform workspace is created the workflow will take the `default` workspace.
Keep in mind this when working with the state and the resources.

This is also reflected on the pipeline from gitlab.

4. **Running locally**

A. Configure the following environment variables from the above service principal created:
```
export ARM_CLIENT_ID=<AppClientIdValue>
export ARM_CLIENT_SECRET=<AppClientSecretValue>
export ARM_TENANT_ID=<TenantId>
export ARM_SUBSCRIPTION_ID=<SubscriptionId>
```
B. Login to azure (install az cli)
- `az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID` or `az login` to work with your AAD user for kubectl actions later. 

C. Go to `aks-cluster/terraform` and execute:

- Init the providers:
 ```
terraform init
```

- Check the plan
```
terraform plan -var client_id=$ARM_CLIENT_ID -var client_secret=$ARM_CLIENT_SECRET -var subscription_id=$ARM_SUBSCRIPTION_ID -var tenant_id=$ARM_TENANT_ID
```

- Apply the plan
```
terraform apply -var client_id=$ARM_CLIENT_ID -var client_secret=$ARM_CLIENT_SECRET -var subscription_id=$ARM_SUBSCRIPTION_ID -var tenant_id=$ARM_TENANT_ID
```

D. When finished the cluster you will see:
- An AKS cluster with AAD RBAC integration with K8s native RBAC. [To connect to the cluster read the next point](https://gitlab.com/b.garcia.l/aks-cluster#aad-rbac-integration-with-k8s-native-rbac) 

- A development namespace and the hello-kubernetes app deployed inside it.

-----

## AAD RBAC Integration with K8s Native RBAC

For provisioning the cluster I am using the [Azure Terraform module for AKS cluster](https://registry.terraform.io/modules/Azure/aks/azurerm/7.4.0?tab=inputs). 
These are the parameters that allow **the AAD RBAC** `<--->` **K8S native RBAC integration**: 


| Azure Terraform Module Parameter                | azurerm_kubernetes_cluster equivalent                      | Description                                                                                                                            |
|-------------------------------------------------|-----------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| `rbac_aad = true`                               | `azure_active_directory_role_based_access_control`        | Enables the integration of AAD with Kubernetes RBAC. It allows using Azure AD identities for access to the Kubernetes API server. So we can use AAD users and AAD groups objectId's in cluster role bindings. |
| `role_based_access_control_enabled = true`      | `rbac_enabled`                                            | Enables native Kubernetes RBAC in the cluster.                                                                                         |
| `rbac_aad_azure_rbac_enabled = true`            | Combination of `role_based_access_control_enabled` and `azure_active_directory_role_based_access_control` | When set to true, enables Azure  RBAC for K8s authorization, allowing use of Azure AD-based Role-Based Access Control (RBAC) for the AKS cluster. This allow the setup of Azure role assignments on the Azure portal (or via terraform) to users and service principals, providing them with roles like **Azure Kubernetes Service RBAC Cluster Role Admin** or others that are usually assigned via K8s `(Cluster)-Roles` and `(Cluster)-RoleBindings`. |
| `rbac_aad_managed = true`                       | `azure_active_directory_role_based_access_control.managed` | Indicates Azure AD integration is managed by Azure, automating the management of Azure AD client and server app details.               |

After creating the AKS cluster with Terraform, I've created the **Azure Kubernetes Service RBAC Cluster Admin** role assignment with:
- my AAD user
- The service principal that is used from Gitlab(az-kubelogin and terraform) to talk to Azure
In this way the pipeline and my user are able to interact with the AKS cluster through the Azure portal and via kubectl
(sending request to K8s API server) from my local machine:

![Azure RBAC ClusterAdmin Role for AKS](https://cldup.com/O3IT76bQSa.png "aad user and sp")

This was possible, due to the above `rbac_aad_azure_rbac_enabled = true` parameter. 
However, is better to automate this by creating `ClusterRoles`, `ClusterRoleBinding` and associating it to either an AAD user or an AAD group. 

[This workflow](https://learn.microsoft.com/en-us/azure/aks/azure-ad-rbac?tabs=portal) can be automated from terraform. 
