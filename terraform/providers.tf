terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.79.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.23.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.11.0"
    }
    random = {
      source = "hashicorp/random"
    }
    http = {
      source = "hashicorp/http"
    }
  }
}

provider "azurerm" {
  # Configuration options
  features {}
  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
}

provider "kubernetes" {
  host                   = data.azurerm_kubernetes_cluster.my-aks.kube_config.0.host
  client_certificate     = base64decode(data.azurerm_kubernetes_cluster.my-aks.kube_config.0.client_certificate)
  client_key             = base64decode(data.azurerm_kubernetes_cluster.my-aks.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.my-aks.kube_config.0.cluster_ca_certificate)
  config_path            = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    host                   = data.azurerm_kubernetes_cluster.my-aks.kube_config.0.host
    client_certificate     = base64decode(data.azurerm_kubernetes_cluster.my-aks.kube_config.0.client_certificate)
    client_key             = base64decode(data.azurerm_kubernetes_cluster.my-aks.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.my-aks.kube_config.0.cluster_ca_certificate)
    config_path            = "~/.kube/config"
  }
}

terraform {
  backend "azurerm" {
    resource_group_name  = "aks-terraform-states"
    storage_account_name = "aksterraform9"
    container_name       = "aks-tf-state"
    key                  = "terraform.tfstate"
  }
}
