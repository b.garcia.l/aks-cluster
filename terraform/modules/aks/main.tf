resource "azurerm_resource_group" "aks_rg" {
  name     = "bgl-aks"
  location = var.aks_location
  tags     = local.tags
}

data "http" "myip" {
  url = "https://ipv4.icanhazip.com"
}


module "azure_aks" {
  source                = "Azure/aks/azurerm"
  version               = "7.4.0"
  cluster_name          = local.kubernetes_cluster_name
  resource_group_name   = azurerm_resource_group.aks_rg.name
  prefix                = local.kubernetes_cluster_name #  dns_prefix
  kubernetes_version    = var.k8s_version
  log_analytics_workspace_enabled = true
  log_analytics_workspace_sku = "PerGB2018"
  log_retention_in_days = 30
  # cluster_log_analytics_workspace_name = azurerm_log_analytics_workspace.eck-monitor.name

  identity_type =  "SystemAssigned" 

  # To associate Azure AD identities with Kubernetes service accounts.
  # allowing applications running on AKS to easily access other Azure resources
  # using the identity that has been assigned to the service account.
  # This feature provides Kubernetes Pods the ability to
  # request and be assigned Azure AD-based identities, which can be used to interact with 
  # Azure services like Azure Key Vault, Azure SQL DB, etc.
  # workload_identity_enabled = true

  node_pools = {
    default_node_pool = {
        name                 = "default"
        node_count           = var.k8s_node_count
        vm_size              = var.k8s_node_size
        zones   = ["1", "2", "3"]
        enable_auto_scaling  = true
        min_count            = 1
        max_count            = 6
        max_pods             = 250
        os_disk_size_gb      = 128
        os_sku               = "Ubuntu"
        type                 = "VirtualMachineScaleSets"
        # vnet_subnet_id       = var.vnet_subnet_id
        # Code="InvalidParameter" Message="Cannot use a custom subnet because agent pool nodepool is 
        # using a managed subnet. Please omit the vnetSubnetID parameter from the re
        orchestrator_version = var.k8s_orchestrator_version
    }
    # another node pool
  }
  
  network_plugin     = "azure"
  load_balancer_sku = "standard"
  load_balancer_profile_enabled = true

  # Allowing K8s native RBAC associated to AAD users and groups.
  rbac_aad = true

  # Enable K8s native RBAC in the cluster
  role_based_access_control_enabled = true

  
  # This integrates Azure AD with Kubernetes RBAC. 
  # It allows me to use Azure AD identities for access to the
  # Kubernetes API server via azure role assignments
  rbac_aad_azure_rbac_enabled = true


  # rbac_aad_managed: When this is set to true, it indicates that the Azure AD integration is managed by 
  # Azure itself. I won't need to specify the Azure AD client and server app details manually; 
  # Azure will manage those details for me. 
  rbac_aad_managed   = true
  # Legacy AAD integration not allowed anymore - https://learn.microsoft.com/en-us/azure/aks/azure-ad-integration-cli
  # rbac_aad_tenant_id = var.tenant_id
  # rbac_aad_client_app_id = var.client_id
  # rbac_aad_server_app_id = var.client_id
  # rbac_aad_server_app_secret = var.client_secret

  tags                       = var.tags
}
