variable "cluster_id" {
  description = "The ID of the AKS cluster"
  type        = string
}
