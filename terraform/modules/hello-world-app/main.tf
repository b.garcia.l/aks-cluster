resource "kubernetes_service" "hello_kubernetes" {
  metadata {
    name      = "hello-kubernetes-service"
    namespace = "development"
  }

  spec {
    selector = {
      app = "hello-kubernetes"
    }

    port {
      port        = 80
      target_port = 8080
    }
  }
}

resource "kubernetes_deployment" "hello_kubernetes" {
  metadata {
    name      = "hello-kubernetes"
    namespace = "development"
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "hello-kubernetes"
      }
    }

    template {
      metadata {
        labels = {
          app = "hello-kubernetes"
        }
      }

      spec {
        container {
          image = "paulbouwer/hello-kubernetes:1.10.1"
          name  = "hello-kubernetes"

          port {
            container_port = 8080
          }

          env {
            name  = "MESSAGE"
            value = "Hello from the cluster!"
          }
          env {
            name = "POD_NAME"
            value_from {
              field_ref {
                field_path = "metadata.name"
              }
            }
          }
          env {
            name = "POD_NAMESPACE"
            value_from {
              field_ref {
                field_path = "metadata.namespace"
              }
            }
          }
          env {
            name = "POD_IP"
            value_from {
              field_ref {
                field_path = "status.podIP"
              }
            }
          }
        }
      }
    }
  }
}
