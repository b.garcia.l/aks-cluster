data "azurerm_kubernetes_cluster" "my-aks" {
  name                = module.aks.kubernetes_cluster_name
  resource_group_name = module.aks.resource_group_name
  depends_on          = [module.aks]
}

#data "azurerm_lb" "aks_lb" {
#  name                = "kubernetes"
#  resource_group_name = "mc_bgl-aks_stag-bgl-aks_westeurope"
#  depends_on = [ module.aks ]
#}

data "http" "myip" {
  url = "https://ipv4.icanhazip.com"
}