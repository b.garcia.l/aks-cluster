resource "kubernetes_namespace" "namespaces" {
  for_each = toset(var.k8s_namespaces)
  metadata {
    name = each.value
  }
}

module "hello-world" {
  source     = "./modules/hello-world-app"
  cluster_id = module.aks.cluster_id # implicit dependency
}

